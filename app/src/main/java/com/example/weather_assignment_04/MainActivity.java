package com.example.weather_assignment_04;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import java.util.List;
import java.util.Locale;
public class MainActivity extends AppCompatActivity {


    private TextView city_name, temperature, wind;
    private TextView humidity;
    private TextView counter;
    private TextView win_dir;

    private TextView cloud;
    private TextInputEditText cityEdt;
    private ImageView search;
    private double lat, longitude;
    private LocationManager locationManager;
    private int PERMISSION_CODE = 1;
    private String cityName;
    private FusedLocationProviderClient location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);


        setContentView(R.layout.activity_main);


        city_name = findViewById(R.id.idCityName);
        temperature = findViewById(R.id.idIVTemperature);
        cityEdt = findViewById(R.id.idEdtCity);

        humidity = findViewById(R.id.idtemp1);
        counter = findViewById(R.id.temp_2);
        win_dir = findViewById(R.id.idtemp3);
        cloud = findViewById(R.id.idtemp5);
        search = findViewById(R.id.idIVSearch);
        wind = findViewById(R.id.idtemp);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        location = LocationServices.getFusedLocationProviderClient(this);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_CODE);

        }


        //Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        location.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    lat = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }
        });
        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(longitude, lat, 100);//10 is maximum no of result
            for (Address address_temp : addresses) {
                String city = address_temp.getLocality();
                cityName = city;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        getWatherinfo(cityName);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String city = cityEdt.getText().toString();
                if (city.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Please enter city name", Toast.LENGTH_SHORT).show();

                } else {
                    city_name.setText(cityName);
                    getWatherinfo(city);
                }
            }
        });

    }





    private void getWatherinfo(String cityName) {

        String url = "http://api.weatherapi.com/v1/forecast.json?key=7fad2d8597594a7082a171605211812&q=" + cityName + "&days=1&aqi=yes&alerts=yes";
        city_name.setText(cityName);
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String coun = response.getJSONObject("location").getString("country");
                    counter.setText(coun);
                    String windtv = response.getJSONObject("current").getString("wind_kph");
                    wind.setText("Wind Speed: " + windtv + "Km/h");
                    String temperature = response.getJSONObject("current").getString("temp_c");
                    MainActivity.this.temperature.setText(temperature + "°c");
                    String humd = response.getJSONObject("current").getString("humidity");
                    humidity.setText("Humidity: " + humd);
                    String dir = response.getJSONObject("current").getString("wind_dir");
                    win_dir.setText("Wind Direction: " + dir);

                    String cloud1 = response.getJSONObject("current").getString("cloud");
                    cloud.setText("Clouds: " + cloud1);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Not valid city", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);


    }


}